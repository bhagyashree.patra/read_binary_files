#!/usr/bin/env python3

import os

print(os.getcwd())
print(os.path.basename(os.getcwd()))
print(os.listdir())
# os.rmdir('files')
# print(os.listdir())
# os.mkdir('octal_dump')
# print(os.listdir())
os.chdir('octal_dump')
print(os.listdir())

f = open('bin.txt')
# f.write('To print the “test” file content in octal format, use the “-b” option:\nod -b filename\n\nHello, third world\nHello, fourth world')
print(f.read())
