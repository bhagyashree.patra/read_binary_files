#!/usr/bin/env python3

# open, write, read, close test file
# read, write or append mode can't be used altogether(one at a time)
# bydefault, file is opened in read mode, considers it as a text file and closed in the end

# write
f = open('new.txt', mode='w', encoding='utf-8')
# or f = open('new.txt', 'wt')
# or f = open('new.txt', 'w')
f.write('Hello World, welcome to the world of Python!\nThis is my second line.\nPracticing readline.\nLast line.')

# read
f = open('new.txt', mode='r', encoding='utf-8')
# or f = open('new.txt')
# r = f.read() # read entire file
r1 = f.read(5) # read first 5 characters
r2 = f.read(5) # read next 5 characters including the space
r3 = f.read(5) # read next 5 characters
r4 = f.read() # read rest of the file
r5 = f.read() # empty string for further reading

print(r1+'\n'+r2+'\n'+r3+'\n'+r4+'\n'+r5)

# readline
f = open('new.txt')
r1 = f.readline() # read first line
r2 = f.readline() # read second line
r3 = f.readline() # read third line
r4 = f.readline() # read fourth line
r5 = f.readline() # will pass nothing
r6 = f.readlines() # empty list as nothing is left

print(r1, r2, r3, r4, r5, r6)

# readlines
f = open('new.txt')
r1 = f.readline() # read first line
r2 = f.readline() # read second line
r6 = f.readlines() # read rest of the lines but as a list

print(r6)
print(r1, r2, r6)

f = open('new.txt')
r6 = f.readlines() # read the entire file as a list
print(r6)

# read a file line by line using for loop
f = open('new.txt')

for line in f:
    print(line, end='')

t1 = f.tell()
s = f.seek(12)
t2 = f.tell()


print(t1)
print(s)
print(t2)

f.close()