To print the “test” file content in octal format, use the “-b” option:
    od -b filename

-i : decimal
-x : hexadecimal
-c : character

“-An” flag with “-c” option will print “test.txt” file content in character format but with no offset information/address (No address)
    od -An -c filename


